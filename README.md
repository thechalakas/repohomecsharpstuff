# repohomeCsharpstuff

this is a linking document for repositories that are stored in my 
bitbucket account. 

This document links to C sharp.

**here are the repos**

I have written some code related c sharp. most of these are console programs intended for those who are learning dot net and c sharp for the firs time.

---

Enums

1. [https://bitbucket.org/thechalakas/enums_1]

2. [https://bitbucket.org/thechalakas/enums_2]

3. [https://bitbucket.org/thechalakas/enums_3]

4. [https://bitbucket.org/thechalakas/enums_4]


---

Classes and Interfaces

1. [https://bitbucket.org/thechalakas/abstract_sealed_classes_1]

2. [https://bitbucket.org/thechalakas/base_derived_interfaces_inheritance_1]

3. [https://bitbucket.org/thechalakas/class_1]

4. [https://bitbucket.org/thechalakas/explicit_interfaces_1]

5. [https://bitbucket.org/thechalakas/interface_1]

---

Types

1. [https://bitbucket.org/thechalakas/boolean_stuff_1]

2. [https://bitbucket.org/thechalakas/boxing_and_unboxing]

3. [https://bitbucket.org/thechalakas/boxing_unboxing_1]

4. [https://bitbucket.org/thechalakas/extending_existing_types]

5. [https://bitbucket.org/thechalakas/fields_and_repositories_1]

6. [https://bitbucket.org/thechalakas/nullables_generics_1]

7. [https://bitbucket.org/thechalakas/securestring_1]

8. [https://bitbucket.org/thechalakas/standard_interfaces_1]

9. [https://bitbucket.org/thechalakas/stringbuilder_1]

10. [https://bitbucket.org/thechalakas/string_enumeration_1]

11. [https://bitbucket.org/thechalakas/string_formatting_1]

12. [https://bitbucket.org/thechalakas/strings_search_1]

13. [https://bitbucket.org/thechalakas/string_tostring_1]

14. [https://bitbucket.org/thechalakas/types_1]

15. [https://bitbucket.org/thechalakas/types_2]

16. [https://bitbucket.org/thechalakas/types_conversions_1]

17. [https://bitbucket.org/thechalakas/typesdiscussion1]

18. [https://bitbucket.org/thechalakas/types_methods_1]

---

Tasks and Threads

1. [https://bitbucket.org/thechalakas/cancellation_task_1]

2. [https://bitbucket.org/thechalakas/tasks_1]

3. [https://bitbucket.org/thechalakas/tasks_2]

4. [https://bitbucket.org/thechalakas/tasks_3]

5. [https://bitbucket.org/thechalakas/threads_1]

6. [https://bitbucket.org/thechalakas/threads_2]

7. [https://bitbucket.org/thechalakas/threads_3]

8. [https://bitbucket.org/thechalakas/threads_4]

---

Collections and Lists and Data and LINQ

1. [https://bitbucket.org/thechalakas/collections_1]

2. [https://bitbucket.org/thechalakas/collections_2]

3. [https://bitbucket.org/thechalakas/collections_use_1]

4. [https://bitbucket.org/thechalakas/json_xml_validation_1]

5. [https://bitbucket.org/thechalakas/linq_stuff_1]

6. [https://bitbucket.org/thechalakas/lock_1]

7. [https://bitbucket.org/thechalakas/parallel_query_1]

8. [https://bitbucket.org/thechalakas/regular_expressions_1]

9. [https://bitbucket.org/thechalakas/serialize_deserialize_1]

10. [https://bitbucket.org/thechalakas/working_with_database_1]

11. [https://bitbucket.org/thechalakas/working_with_files_directories_1]

12. [https://bitbucket.org/thechalakas/working_with_files_directory_tree_1]

13. [https://bitbucket.org/thechalakas/working_with_files_drives_1]

14. [https://bitbucket.org/thechalakas/working_with_files_file_manipulation_1]

15. [https://bitbucket.org/thechalakas/working_with_streams_1]

16. [https://bitbucket.org/thechalakas/xml_stuff_1]

Web Call Stuff

1. [https://bitbucket.org/thechalakas/webrequest_webresponse_1]

General Concepts

1. [https://bitbucket.org/thechalakas/anonymousstuff1]

2. [https://bitbucket.org/thechalakas/asyn_await_1]

3. [https://bitbucket.org/thechalakas/certificates_1]

4. [https://bitbucket.org/thechalakas/decision_stuff_1]

5. [https://bitbucket.org/thechalakas/delegates_1]

6. [https://bitbucket.org/thechalakas/encryption_1]

7. [https://bitbucket.org/thechalakas/events_1]

8. [https://bitbucket.org/thechalakas/exceptions_1]

9. [https://bitbucket.org/thechalakas/extensions_overriding_1]

10. [https://bitbucket.org/thechalakas/hashing_1]

11. [https://bitbucket.org/thechalakas/input_validation_1]

12. [https://bitbucket.org/thechalakas/is_as_1]

13. [https://bitbucket.org/thechalakas/method_overloading]

14. [https://bitbucket.org/thechalakas/overriding_methods]

15. [https://bitbucket.org/thechalakas/parallel_1]

16. [https://bitbucket.org/thechalakas/parse_tryparse_1]

17. [https://bitbucket.org/thechalakas/wcf_service_1]

Others

1. [https://bitbucket.org/thechalakas/utlpracticejuly6thb/src]

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 